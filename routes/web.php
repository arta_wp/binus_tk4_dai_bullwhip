<?php

use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('role', RoleController::class);
Route::post('/role/store', [RoleController::class, 'store']);

Route::resource('user', UserController::class);

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/pegawai', [App\Http\Controllers\PegawaiController::class, 'index'])->name('pegawai');
Route::post('/pegawai', [App\Http\Controllers\PegawaiController::class, 'create'])->name('pegawai.create');
Route::get('/pegawai/getDatatable', [App\Http\Controllers\PegawaiController::class, 'getDatatable'])->name('pegawai.getDatatable');
Route::post('/pegawai/change-password/{id}', [App\Http\Controllers\PegawaiController::class, 'change_password'])->name('pegawai.change_password');
Route::get('/pegawai/{id}', [App\Http\Controllers\PegawaiController::class, 'show'])->name('pegawai.show');
Route::put('/pegawai/{id}', [App\Http\Controllers\PegawaiController::class, 'update'])->name('pegawai.update');
Route::delete('/pegawai/{id}', [App\Http\Controllers\PegawaiController::class, 'delete'])->name('pegawai.delete');

Route::get('/barang', [App\Http\Controllers\BarangController::class, 'index'])->name('barang');
Route::post('/barang', [App\Http\Controllers\BarangController::class, 'create'])->name('barang.create');
Route::get('/barang/getDatatable', [App\Http\Controllers\BarangController::class, 'getDatatable'])->name('barang.getDatatable');
Route::post('/barang/change-password/{id}', [App\Http\Controllers\BarangController::class, 'change_password'])->name('barang.change_password');
Route::get('/barang/{id}', [App\Http\Controllers\BarangController::class, 'show'])->name('barang.show');
Route::put('/barang/{id}', [App\Http\Controllers\BarangController::class, 'update'])->name('barang.update');
Route::delete('/barang/{id}', [App\Http\Controllers\BarangController::class, 'delete'])->name('barang.delete');

Route::get('/bagian', [App\Http\Controllers\BagianController::class, 'index'])->name('bagian');
Route::post('/bagian', [App\Http\Controllers\BagianController::class, 'create'])->name('bagian.create');
Route::get('/bagian/getDatatable', [App\Http\Controllers\BagianController::class, 'getDatatable'])->name('bagian.getDatatable');
Route::post('/bagian/change-password/{id}', [App\Http\Controllers\BagianController::class, 'change_password'])->name('bagian.change_password');
Route::get('/bagian/{id}', [App\Http\Controllers\BagianController::class, 'show'])->name('bagian.show');
Route::put('/bagian/{id}', [App\Http\Controllers\BagianController::class, 'update'])->name('bagian.update');
Route::delete('/bagian/{id}', [App\Http\Controllers\BagianController::class, 'delete'])->name('bagian.delete');


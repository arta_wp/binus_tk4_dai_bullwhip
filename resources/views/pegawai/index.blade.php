@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h4 class="page-title my-4 fw-bold">Pegawai</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row justify-content-between mb-3">
                            <div class="col-8">
                                <h5 class=" mb-4">Daftar Pegawai</h5>
                            </div>
                            <div class="col-4">
                                <div class="float-end mt-md-0">
                                    <button type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalAddPegawai">
                                        <span class="btn-label"><i class="fa fa-plus"></i> </span>Tambah Pegawai
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="table-pegawai" class="table nowrap w-100">
                                <thead class="table-light">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th>Username</th>
                                        <th>Nama Pegawai</th>
                                        <th>Alamat</th>
                                        <th>No. Handphone</th>
                                        <th>Bagian</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalAddPegawai" data-bs-backdrop="static" aria-labelledby="modalAddPegawaiLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="px-3" id="form-add">
                    <div class="modal-header" style="border: none;">
                        <h5 class="modal-title">Form Input Pegawai</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="add_nama_pegawai" class="form-label">Nama Pegawai</label>
                            <input class="form-control" type="text" id="add_nama_pegawai" name="add_nama_pegawai" placeholder="Nama Pegawai" required>
                        </div>
                        <div class="mb-3">
                            <label for="add_alamat" class="form-label">Alamat Pegawai</label>
                            <textarea class="form-control" name="add_alamat" id="add_alamat" cols="30" rows="3" placeholder="Alamat Pegawai" required></textarea>
                        </div>
                        <div class="mb-3">
                            <label for="add_phone" class="form-label">Handphone Pegawai</label>
                            <input class="form-control" type="text" id="add_phone" name="add_phone" placeholder="Handphone Pegawai" required>
                        </div>
                        <div class="mb-3">
                            <label for="add_username" class="form-label">Username</label>
                            <input type="text" id="add_username" name="add_username" class="form-control" placeholder="Username" autocomplete="off" required>
                        </div>
                        <div class="mb-3">
                            <label for="add_password" class="form-label">Password</label>
                            <input type="text" id="add_password" name="add_password" class="form-control" placeholder="Password" required>
                        </div>
                        <div class="mb-3">
                            <label for="add_bagian" class="form-label">Bagian</label>
                            <select class="form-control" id="add_bagian" name="add_bagian" required>
                                <option value="">Pilih Bagian...</option>
                                @foreach($bagians as $bagian)
                                    <option value="{{ $bagian->id_bagian }}">{{ $bagian->nama_bagian }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer" style="border: none;">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="btn-add">
                            <i class="fe-save mr-1"></i> Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEditPegawai" data-bs-backdrop="static" aria-labelledby="modalEditPegawaiLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="px-3" id="form-edit">
                    <div class="modal-header" style="border: none;">
                        <h5 class="modal-title">Form Edit Pegawai</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="edit_nama_pegawai" class="form-label">Nama Pegawai</label>
                            <input class="form-control" type="text" id="edit_nama_pegawai" name="edit_nama_pegawai" placeholder="Nama Pegawai" required>
                        </div>
                        <div class="mb-3">
                            <label for="edit_alamat" class="form-label">Alamat Pegawai</label>
                            <textarea class="form-control" name="edit_alamat" id="edit_alamat" cols="30" rows="3" placeholder="Alamat Pegawai" required></textarea>
                        </div>
                        <div class="mb-3">
                            <label for="edit_phone" class="form-label">Handphone Pegawai</label>
                            <input class="form-control" type="text" id="edit_phone" name="edit_phone" placeholder="Handphone Pegawai" required>
                        </div>
                        <div class="mb-3">
                            <label for="edit_username" class="form-label">Username</label>
                            <input type="text" id="edit_username" name="edit_username" class="form-control" placeholder="Username" autocomplete="off" required>
                        </div>
                        <div class="mb-3">
                            <label for="edit_bagian" class="form-label">Bagian</label>
                            <select class="form-control" id="edit_bagian" name="edit_bagian" required>
                                <option value="">Pilih Bagian...</option>
                                @foreach($bagians as $bagian)
                                    <option value="{{ $bagian->id_bagian }}">{{ $bagian->nama_bagian }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer" style="border: none;">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="btn-edit">
                            <i class="fe-save mr-1"></i> Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalChangePassword" data-bs-backdrop="static" aria-labelledby="modalEditPegawaiLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="px-3" id="form-change-password">
                    <div class="modal-header" style="border: none;">
                        <h5 class="modal-title">Form Ganti Password</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="new_password" class="form-label">Password Baru</label>
                            <input class="form-control" type="text" id="new_password" name="new_password" placeholder="Masukkan Password Baru" required>
                        </div>
                    </div>
                    <div class="modal-footer" style="border: none;">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="btn-change-password">
                            <i class="fe-save mr-1"></i> Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script-bottom')
<script>
    let datatablePegawai;
    let edit_id=null;

    $(document).ready(function () {
        datatablePegawai = $('#table-pegawai').DataTable({
            searchDelay: 500,
            processing: true,
            serverSide: true,
            order: [[ 1]],
            ajax: baseUrl + "/pegawai/getDatatable",
            columns: [
                { data: 'DT_RowIndex' },
                { data: 'username' },
                { data: 'nama_pegawai' },
                { data: 'alamat_pegawai' },
                { data: 'hp_pegawai' },
                { data: 'bagian.nama_bagian' },
                { data: 'actions' }
            ],
            columnDefs: [
                {
                    targets: 0,
                    searchable: false,
                    orderable: false,
                    className: "text-center",
                    width: '7%'
                },
                {
                    targets: -1,
                    searchable: false,
                    orderable: false,
                    className: "text-center"
                },
            ]
        });

        $("#add_bagian").select2({
            placeholder: 'Pilih Bagian...',
            width: '100%',
            dropdownParent: $("#modalAddPegawai")
        });

        $("#edit_bagian").select2({
            placeholder: 'Pilih Bagian...',
            width: '100%',
            dropdownParent: $("#modalEditPegawai")
        });

        $('#form-add').submit(function(e) {
            e.preventDefault();

            $.ajax({
                url: baseUrl + "/pegawai",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': token
                },
                data: $(this).serialize(),
                dataType: "JSON",
                beforeSend: function () {
                    beforeLoading('#btn-add');
                },
                success: function (result) {
                    afterLoading('#btn-add', 'Simpan');
                    if (result.metaData.code === 200) {
                        $('#modalAddPegawai').modal('hide');
                        messages('Data berhasil disimpan.', null);
                        datatablePegawai.ajax.reload();
                    } else {
                        Swal.fire(
                            'Perhatian!',
                            result.metaData.message,
                            'warning'
                        )
                    }
                },
                error: function (eventError, textStatus, errorThrown) {
                    afterLoading('#btn-add', 'Simpan');
                    Swal.fire(
                        'Terjadi Kesalahan!',
                        formatErrorMessage(eventError, errorThrown),
                        'error'
                    )
                }
            });
        });

        $('#form-edit').submit(function(e) {
            e.preventDefault();

            $.ajax({
                url: baseUrl + "/pegawai/" + edit_id,
                type: "PUT",
                headers: {
                    'X-CSRF-TOKEN': token
                },
                data: $(this).serialize(),
                dataType: "JSON",
                beforeSend: function () {
                    beforeLoading('#btn-edit');
                },
                success: function (result) {
                    afterLoading('#btn-edit', 'Simpan');
                    if (result.metaData.code === 200) {
                        $('#modalEditPegawai').modal('hide');
                        messages('Data berhasil disimpan.', null);
                        datatablePegawai.ajax.reload();
                    } else {
                        Swal.fire(
                            'Perhatian!',
                            result.metaData.message,
                            'warning'
                        )
                    }
                },
                error: function (eventError, textStatus, errorThrown) {
                    afterLoading('#btn-edit', 'Simpan');
                    Swal.fire(
                        'Terjadi Kesalahan!',
                        formatErrorMessage(eventError, errorThrown),
                        'error'
                    )
                }
            });
		});

        $('#form-change-password').submit(function(e) {
            e.preventDefault();

            $.ajax({
                url: baseUrl + "/pegawai/change-password/" + edit_id,
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': token
                },
                data: $(this).serialize(),
                dataType: "JSON",
                beforeSend: function () {
                    beforeLoading('#btn-change-password');
                },
                success: function (result) {
                    afterLoading('#btn-change-password', 'Update');
                    if (result.metaData.code === 200) {
                        $('#modalChangePassword').modal('hide');
                        messages('Password berhasil diupdate.', null);
                    } else {
                        Swal.fire(
                            'Perhatian!',
                            result.metaData.message,
                            'warning'
                        )
                    }
                },
                error: function (eventError, textStatus, errorThrown) {
                    afterLoading('#btn-change-password', 'Update');
                    Swal.fire(
                        'Terjadi Kesalahan!',
                        formatErrorMessage(eventError, errorThrown),
                        'error'
                    )
                }
            });
		});
    });

    window.editPegawai = input => {

        var id = $(input).attr('data');
        $.ajax({
            url: baseUrl + "/pegawai/" + id,
            type: "GET",
            headers: {
                'X-CSRF-TOKEN': token
            },
            dataType: "JSON",
            beforeSend: function () {
                
            },
            success: function (result) {
                Swal.close();
                if (result.metaData.code === 200) {
                    $('#modalEditPegawai').modal('show');
                    edit_id = result.response.id_pegawai;
                    $('#edit_nama_pegawai').val(result.response.nama_pegawai);
                    $('#edit_alamat').val(result.response.alamat_pegawai);
                    $('#edit_phone').val(result.response.hp_pegawai);
                    $('#edit_username').val(result.response.username);
                    $('#edit_bagian').val(result.response.id_bagian).trigger("change");

                } else {
                    Swal.fire(
                        'Perhatian!',
                        result.metaData.message,
                        'warning'
                    )
                }
            },
            error: function (eventError, textStatus, errorThrown) {
                Swal.close();
                Swal.fire(
                    'Terjadi Kesalahan!',
                    formatErrorMessage(eventError, errorThrown),
                    'error'
                )
            }
        });
    }

    window.deletePegawai = input => {
        var id = $(input).attr('data');
        
        Swal.fire({
            title: 'Hapus Data',
            icon: "warning",
            text: "Anda yakin akan menghapus pegawai tersebut?",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya (Hapus)',
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
                deleteData('/pegawai/' + id).then(response => {
                if (!response.ok) {
                    Swal.fire(
                        'Perhatian!',
                        'Gagal menghapus data!',
                        'error'
                    )
                }
                return response.json();
            }).then(result => {
                if(result.metaData.code === 200) {
                    messages('Data berhasil dihapus.', null);
                    datatablePegawai.ajax.reload();
                } else {
                    Swal.fire(
                        'Perhatian!',
                        'Gagal menghapus data!',
                        'warning'
                    )
                }
            }).catch(error => {
                Swal.showValidationMessage(
                    `Request failed: ${error}`
                )
            })
            },
            allowOutsideClick: () => !Swal.isLoading()
        }); 
    }

    window.changePassword = input => {
        var id = $(input).attr('data');
        $.ajax({
            url: baseUrl + "/pegawai/" + id,
            type: "GET",
            headers: {
                'X-CSRF-TOKEN': token
            },
            dataType: "JSON",
            beforeSend: function () {
                
            },
            success: function (result) {
                Swal.close();
                if (result.metaData.code === 200) {
                    edit_id = result.response.id_pegawai;
                    $('#modalChangePassword').modal('show');

                } else {
                    Swal.fire(
                        'Perhatian!',
                        result.metaData.message,
                        'warning'
                    )
                }
            },
            error: function (eventError, textStatus, errorThrown) {
                Swal.close();
                Swal.fire(
                    'Terjadi Kesalahan!',
                    formatErrorMessage(eventError, errorThrown),
                    'error'
                )
            }
        });
    }

</script>
@endpush

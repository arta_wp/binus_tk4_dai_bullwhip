'use strict'

import './axios';

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

window.messages = (message, url=null) => {
    Swal.fire({
        title: 'Berhasil',
        text: message,
        icon: 'success',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oke',
        allowOutsideClick: false,
    }).then((result) => {
        if (result.value) {
            if(url !== null) {
                window.location.href = url;
            }
        }
    })
}

window.beforeLoadingAttr = (el, textLoading=null) => {
    $(el).prop('disabled', true);
    if(textLoading === null) {
        $(el).empty().html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span><span class="visually-hidden">Loading...</span>`);
    } else {
        $(el).empty().html(`<span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>${textLoading}`);
    }
}

window.afterLoadingAttr = (el, content) => {
    $(el).prop('disabled', false);
    $(el).empty().html(content);
}

window.getValue = (element) => {
    return document.getElementById(element).value
}

window.isNumber = evt => {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}

window.getRadioValue = (element) => {
    var radios = document.getElementsByName(element);
    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            return radios[i].value;
        }
    }
}

window.limitInput = (element, limit) => {
    return $(element).on("keyup", ({ target }) => {
        let { value } = target;
        console.log(value)
        return ($(element).value = value.slice(
            0,
            limit
        ));
    });
};

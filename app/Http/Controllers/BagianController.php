<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Bagian;
use DB;

class BagianController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('bagian.index');
    }

    public function show($id)
    {
        $result = Bagian::find($id);

        if (is_null($result)) {
            return response()->json(['metaData' => ['code' => 404, 'message' => 'Bagian tidak ditemukan.'], 'response' => null], 200);
        } else {
            return response()->json(['metaData' => ['code' => 200, 'message' => 'OK'], 'response' => $result], 200);
        }
    }

    public function getDatatable()
    {
        $bagian = Bagian::orderBy('nama_bagian');

        return Datatables::eloquent($bagian)
            ->addIndexColumn()
            ->addColumn('actions', function($bagian) {
                return '<button type="button" class="btn btn-success btn-sm waves-effect waves-light" data="'.$bagian->id_bagian.'" onclick="editBagian(this,event)"><i class="fa fa-pencil"></i> Edit</button>
                <button type="button" class="btn btn-danger btn-sm waves-effect waves-light" data="'.$bagian->id_bagian.'" onclick="deleteBagian(this,event)"><i class="fa fa-trash"></i> Hapus</button>
                ';
            })
            ->rawColumns(['actions'])
            ->toJson();
    }

    public function create(Request $request)
    {
        $check_bagian = Bagian::where('nama_bagian', $request->get('add_nama_bagian'))->first();

        if(!is_null($check_bagian)) {
            return response()->json(['metaData' => ['code' => 402,'message' => 'Nama Bagian '. $request->get('add_nama_bagian') .' sudah terdaftar.'], 'response' => null], 200); 
        }

        DB::beginTransaction();

        try
        {
            $bagian = Bagian::create([
                'nama_bagian' => $request->get('add_nama_bagian'),
            ]);

            DB::commit();

            return response()->json(['metaData' => ['code' => 200,'message' => 'Data berhasil disimpan.'],'response' => $bagian], 200); 
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return response(['message'=> $e->getMessage()], 500);
        }

    }

    public function update($id, Request $request)
    {
        $result = Bagian::find($id);

        if (is_null($result)) {
            return response()->json(['metaData' => ['code' => 404, 'message' => 'Bagian tidak ditemukan.'], 'response' => null], 200);
        }

        $result->nama_Bagian = $request->get('edit_nama_bagian');

        $result->save();

        return response()->json(['metaData' => ['code' => 200,'message' => 'Data berhasil disimpan.'],'response' => $result], 200); 
    }

    public function delete($id)
    {
        DB::beginTransaction();

        try
        {
            $result = Bagian::find($id);

            $result->delete();

            DB::commit();

            return response()->json(['metaData' => ['code' => 200, 'message' => 'Data berhasil dihapus.'],'response' => $result], 200); 
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return response(['message'=> $e->getMessage()], 500);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Pegawai;
use App\Models\Bagian;
use DB;
use Hash;

class PegawaiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $bagians = Bagian::orderBy('nama_bagian')->get();
        return view('pegawai.index')->with('bagians', $bagians);
    }

    public function show($id)
    {
        $result = Pegawai::with('bagian')->find($id);

        if (is_null($result)) {
            return response()->json(['metaData' => ['code' => 404, 'message' => 'Pegawai tidak ditemukan.'], 'response' => null], 200);
        } else {
            return response()->json(['metaData' => ['code' => 200, 'message' => 'OK'], 'response' => $result], 200);
        }
    }

    public function getDatatable()
    {
        $pegawai = Pegawai::with('bagian')->where('id_pegawai', '!=', Auth::user()->id_pegawai);

        return Datatables::eloquent($pegawai)
            ->addIndexColumn()
            ->addColumn('actions', function($pegawai) {
                return '<button type="button" class="btn btn-success btn-sm waves-effect waves-light" data="'.$pegawai->id_pegawai.'" onclick="editPegawai(this,event)"><i class="fa fa-pencil"></i> Edit</button>
                <button type="button" class="btn btn-danger btn-sm waves-effect waves-light" data="'.$pegawai->id_pegawai.'" onclick="deletePegawai(this,event)"><i class="fa fa-trash"></i> Hapus</button>
                <button type="button" class="btn btn-secondary btn-sm waves-effect waves-light" data="'.$pegawai->id_pegawai.'" onclick="changePassword(this,event)"><i class="fa fa-lock"></i> Ganti Password</button>
                ';
            })
            ->rawColumns(['actions'])
            ->toJson();
    }

    public function create(Request $request)
    {
        $check_username = Pegawai::where('username', $request->get('add_username'))->first();

        if(!is_null($check_username)) {
            return response()->json(['metaData' => ['code' => 402,'message' => 'Username '. $request->get('add_username') .' sudah terdaftar.'], 'response' => null], 200); 
        }

        DB::beginTransaction();

        try
        {
            $pegawai = Pegawai::create([
                'username' => $request->get('add_username'),
                'nama_pegawai' => $request->get('add_nama_pegawai'),
                'alamat_pegawai' => $request->get('add_alamat'),
                'hp_pegawai' => $request->get('add_phone'),
                'password' => Hash::make($request->get('add_password')),
                'id_bagian' => $request->get('add_bagian')
            ]);

            DB::commit();

            return response()->json(['metaData' => ['code' => 200,'message' => 'Data berhasil disimpan.'],'response' => $pegawai], 200); 
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return response(['message'=> $e->getMessage()], 500);
        }

    }

    public function update($id, Request $request)
    {
        $result = Pegawai::find($id);
        
        $check_username = Pegawai::where('username', $request->get('edit_username'))->first();

        if(!is_null($check_username) && $check_username->username != $result->username) {
            return response()->json(['metaData' => ['code' => 402,'message' => 'Username '. $request->get('edit_username') .' sudah terdaftar.'],'response' => null], 200); 
        }

        if (is_null($result)) {
            return response()->json(['metaData' => ['code' => 404, 'message' => 'Pegawai tidak ditemukan.'], 'response' => null], 200);
        }

        $result->username = $request->get('edit_username');
        $result->nama_pegawai = $request->get('edit_nama_pegawai');
        $result->alamat_pegawai = $request->get('edit_alamat');
        $result->hp_pegawai = $request->get('edit_phone');
        $result->id_bagian = $request->get('edit_bagian');

        $result->save();

        return response()->json(['metaData' => ['code' => 200,'message' => 'Data berhasil disimpan.'],'response' => $result], 200); 
    }

    public function delete($id)
    {
        DB::beginTransaction();

        try
        {
            $result = Pegawai::find($id);

            $result->delete();

            DB::commit();

            return response()->json(['metaData' => ['code' => 200, 'message' => 'Data berhasil dihapus.'],'response' => $result], 200); 
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return response(['message'=> $e->getMessage()], 500);
        }
    }

    public function change_password($id, Request $request)
    {
        $result = Pegawai::find($id);

        if (is_null($result)) {
            return response()->json(['metaData' => ['code' => 404, 'message' => 'Pegawai tidak ditemukan.'], 'response' => null], 200);
        }

        $result->password = Hash::make($request->get('new_password'));

        $result->save();

        return response()->json(['metaData' => ['code' => 200,'message' => 'Data berhasil disimpan.'],'response' => $result], 200); 
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Barang;
use DB;

class BarangController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('barang.index');
    }

    public function show($id)
    {
        $result = Barang::find($id);

        if (is_null($result)) {
            return response()->json(['metaData' => ['code' => 404, 'message' => 'Barang tidak ditemukan.'], 'response' => null], 200);
        } else {
            return response()->json(['metaData' => ['code' => 200, 'message' => 'OK'], 'response' => $result], 200);
        }
    }

    public function getDatatable()
    {
        $barang = Barang::orderBy('nama_barang');

        return Datatables::eloquent($barang)
            ->addIndexColumn()
            ->addColumn('actions', function($barang) {
                return '<button type="button" class="btn btn-success btn-sm waves-effect waves-light" data="'.$barang->id_barang.'" onclick="editBarang(this,event)"><i class="fa fa-pencil"></i> Edit</button>
                <button type="button" class="btn btn-danger btn-sm waves-effect waves-light" data="'.$barang->id_barang.'" onclick="deleteBarang(this,event)"><i class="fa fa-trash"></i> Hapus</button>
                ';
            })
            ->rawColumns(['actions'])
            ->toJson();
    }

    public function create(Request $request)
    {
        $check_barang = Barang::where('nama_barang', $request->get('add_nama_barang'))->first();

        if(!is_null($check_barang)) {
            return response()->json(['metaData' => ['code' => 402,'message' => 'Nama Barang '. $request->get('add_nama_barang') .' sudah terdaftar.'], 'response' => null], 200); 
        }

        DB::beginTransaction();

        try
        {
            $barang = Barang::create([
                'nama_barang' => $request->get('add_nama_barang'),
            ]);

            DB::commit();

            return response()->json(['metaData' => ['code' => 200,'message' => 'Data berhasil disimpan.'],'response' => $barang], 200); 
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return response(['message'=> $e->getMessage()], 500);
        }

    }

    public function update($id, Request $request)
    {
        $result = Barang::find($id);

        if (is_null($result)) {
            return response()->json(['metaData' => ['code' => 404, 'message' => 'Barang tidak ditemukan.'], 'response' => null], 200);
        }

        $result->nama_barang = $request->get('edit_nama_barang');

        $result->save();

        return response()->json(['metaData' => ['code' => 200,'message' => 'Data berhasil disimpan.'],'response' => $result], 200); 
    }

    public function delete($id)
    {
        DB::beginTransaction();

        try
        {
            $result = Barang::find($id);

            $result->delete();

            DB::commit();

            return response()->json(['metaData' => ['code' => 200, 'message' => 'Data berhasil dihapus.'],'response' => $result], 200); 
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return response(['message'=> $e->getMessage()], 500);
        }
    }
}

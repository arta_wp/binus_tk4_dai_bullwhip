<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        $role = Role::all();

        return \view('role', ['role' => $role]);
    }

    public function create()
    {
        return \view('role_create', ['role' => []]);
    }

    public function edit(Role $role)
    {
        return \view('role_edit', compact('role'));
    }

    public function store(Request $request)
    {
        Role::create($request->all());

        return \redirect('/role');
    }

    public function update(Request $request, Role $role)
    {
        $role->update($request->all());

        return \redirect('/role');
    }

    public function destroy(Role $role)
    {
        $role->delete();

        return \redirect('/role');
    }
}

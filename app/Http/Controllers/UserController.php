<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();

        return \view('user', ['users' => $users]);
    }

    public function create()
    {
        $roles = Role::all();

        return \view('user_create', ['roles' => $roles]);
    }

    public function store(Request $request)
    {
        User::create($request->all());

        return \redirect('/user');
    }

    public function edit(User $user)
    {
        $roles = Role::all();

        return \view('user_edit', ['user' => $user, 'roles' => $roles]);
    }

    public function update(Request $request, User $user)
    {
        $user->update($request->all());

        return \redirect('/user');
    }

    public function destroy(User $user)
    {
        $user->delete();

        return \redirect('/user');
    }
}

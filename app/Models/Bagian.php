<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bagian extends Model
{
    protected $table = 'bagian';
    protected $primaryKey = 'id_bagian';
    public $timestamps = false;

    protected $fillable = [
        'id_bagian',
        'nama_bagian'
    ];

    public function pegawai()
    {
        return $this->hasMany(Pegawai::class);
    }

}

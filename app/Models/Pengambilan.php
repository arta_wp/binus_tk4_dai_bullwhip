<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengambilan extends Model
{
    protected $table = 'pengambilan';
    protected $primaryKey = 'id_pengambilan';

    protected $fillable = [
        'id_pengambilan',
        'nama_pengambil',
        'id_barang',
        'jumlah_pengambilan'
    ];

    public function pegawai()
    {
        return $this->hasMany(Pegawai::class);
    }

}

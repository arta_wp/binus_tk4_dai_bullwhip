/*
 Navicat Premium Data Transfer

 Source Server         : MySQL Local
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : sibangsat

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 31/10/2022 10:05:34
*/

-- ----------------------------
-- Records of barang
-- ----------------------------
BEGIN;
INSERT INTO `barang` VALUES (44, 'advice debet');
INSERT INTO `barang` VALUES (57, 'advice kredit');
INSERT INTO `barang` VALUES (52, 'amplop cokelat besar (E)');
INSERT INTO `barang` VALUES (51, 'amplop coklat folio (D)');
INSERT INTO `barang` VALUES (49, 'amplop coklat kecil (A)');
INSERT INTO `barang` VALUES (50, 'amplop coklat sedang (C)');
INSERT INTO `barang` VALUES (42, 'amplop kabinet kaca');
INSERT INTO `barang` VALUES (41, 'amplop kabinet tanpa kaca');
INSERT INTO `barang` VALUES (38, 'amplop kantong uang besar');
INSERT INTO `barang` VALUES (53, 'amplop kantong uang kecil');
INSERT INTO `barang` VALUES (54, 'amplop kantong uang kecil (bertu');
INSERT INTO `barang` VALUES (55, 'amplop kantong uang sedang (bert');
INSERT INTO `barang` VALUES (56, 'amplop plastik deposito (biru)');
INSERT INTO `barang` VALUES (47, 'amplop putih kabinet');
INSERT INTO `barang` VALUES (48, 'amplop putih kaca kabinet');
INSERT INTO `barang` VALUES (124, 'apl.pemb.rek giro (badan)');
INSERT INTO `barang` VALUES (78, 'apl.pemb.rek. tabungan rencana m');
INSERT INTO `barang` VALUES (114, 'aplikasi &SK perdebetan direct d');
INSERT INTO `barang` VALUES (73, 'aplikasi kartu kredit mandiri');
INSERT INTO `barang` VALUES (74, 'aplikasi keluhan nasabah');
INSERT INTO `barang` VALUES (63, 'Aplikasi pemb.rek.non perorangan');
INSERT INTO `barang` VALUES (62, 'aplikasi pemb.rek.perorangan (BP');
INSERT INTO `barang` VALUES (132, 'aplikasi pemb.rek.prod.dana P (l');
INSERT INTO `barang` VALUES (102, 'aplikasi pembuka deposito');
INSERT INTO `barang` VALUES (31, 'aplikasi pembukaan rekening prod');
INSERT INTO `barang` VALUES (75, 'aplikasi sms banking & internet ');
INSERT INTO `barang` VALUES (129, 'aplikasi tabungan rencana mandir');
INSERT INTO `barang` VALUES (29, 'aplikasi umum');
INSERT INTO `barang` VALUES (35, 'ban uang Rp 100.000');
INSERT INTO `barang` VALUES (36, 'ban uang Rp 50.000');
INSERT INTO `barang` VALUES (113, 'brosur tabungan mandiri');
INSERT INTO `barang` VALUES (25, 'bukti setoran tunai');
INSERT INTO `barang` VALUES (127, 'form aplikasi mandiri internet b');
INSERT INTO `barang` VALUES (64, 'form aplikasi umum');
INSERT INTO `barang` VALUES (128, 'form bill payment / DIRECT DEBET');
INSERT INTO `barang` VALUES (83, 'form bukti penyerahan');
INSERT INTO `barang` VALUES (107, 'form IBT masuk');
INSERT INTO `barang` VALUES (30, 'form keluhan nasabah');
INSERT INTO `barang` VALUES (101, 'form keluhan Visa');
INSERT INTO `barang` VALUES (117, 'form klaim asuransi sinarmas');
INSERT INTO `barang` VALUES (136, 'form konfirmasi penarikan');
INSERT INTO `barang` VALUES (40, 'form kunjungan nasabah mikro');
INSERT INTO `barang` VALUES (99, 'form kunjungan nasabah safe D Bo');
INSERT INTO `barang` VALUES (95, 'form laporam uang palsu,');
INSERT INTO `barang` VALUES (135, 'form mandiri internet bisnis');
INSERT INTO `barang` VALUES (122, 'form mandiri sekuritas');
INSERT INTO `barang` VALUES (27, 'form multi payment');
INSERT INTO `barang` VALUES (96, 'form pembayaran KKN');
INSERT INTO `barang` VALUES (90, 'form pembayaran pajak');
INSERT INTO `barang` VALUES (123, 'form pembayaran pajak (1/2 hal u');
INSERT INTO `barang` VALUES (91, 'form pembayaran telpon');
INSERT INTO `barang` VALUES (120, 'form pembelian, reksa dana');
INSERT INTO `barang` VALUES (119, 'form pembukaan reksa dana mandir');
INSERT INTO `barang` VALUES (134, 'form pemesanan pembelian unit pe');
INSERT INTO `barang` VALUES (26, 'form penarikan');
INSERT INTO `barang` VALUES (121, 'form penjualan kembali reksa dan');
INSERT INTO `barang` VALUES (71, 'form penutupan kartu kredit');
INSERT INTO `barang` VALUES (87, 'form perpanjangan otomatis depos');
INSERT INTO `barang` VALUES (133, 'form praktis \"cab. pandanaran\"');
INSERT INTO `barang` VALUES (39, 'form praktis 2 PLY');
INSERT INTO `barang` VALUES (130, 'form praktis 3 PLY');
INSERT INTO `barang` VALUES (106, 'form SBMPTN');
INSERT INTO `barang` VALUES (105, 'form SUKUK');
INSERT INTO `barang` VALUES (88, 'form tabungan haji');
INSERT INTO `barang` VALUES (89, 'form transaksi antar teller');
INSERT INTO `barang` VALUES (110, 'form transaksi singkat');
INSERT INTO `barang` VALUES (82, 'form wakat penyerahan');
INSERT INTO `barang` VALUES (126, 'form WALK IN CUSTUMER');
INSERT INTO `barang` VALUES (112, 'informasi penggantian buku tabun');
INSERT INTO `barang` VALUES (111, 'informasi penggantian kartu ATM');
INSERT INTO `barang` VALUES (60, 'internal credit (hvs)');
INSERT INTO `barang` VALUES (61, 'internal credit (NCR)');
INSERT INTO `barang` VALUES (58, 'internal Debet (HVS)');
INSERT INTO `barang` VALUES (59, 'internal Debet (NCR)');
INSERT INTO `barang` VALUES (81, 'kartu nama');
INSERT INTO `barang` VALUES (118, 'KMS (money Trans S)');
INSERT INTO `barang` VALUES (43, 'kop surat');
INSERT INTO `barang` VALUES (46, 'kop surat mandiri (hal 2)');
INSERT INTO `barang` VALUES (45, 'kop surat mandiri (hal1)');
INSERT INTO `barang` VALUES (92, 'KTP khusus pengambilan ridho');
INSERT INTO `barang` VALUES (93, 'KTPI Khusus percetakan transaksi');
INSERT INTO `barang` VALUES (131, 'kuitansi');
INSERT INTO `barang` VALUES (67, 'laporan teller pagi');
INSERT INTO `barang` VALUES (68, 'laporan teller pagi (NCR)');
INSERT INTO `barang` VALUES (70, 'laporan teller sore ');
INSERT INTO `barang` VALUES (69, 'laporan teller sore (NCR)');
INSERT INTO `barang` VALUES (109, 'memo setoran tunai');
INSERT INTO `barang` VALUES (94, 'nota pembelian /penjualan paluta');
INSERT INTO `barang` VALUES (72, 'pembayaran kartu kredit mandiri');
INSERT INTO `barang` VALUES (84, 'rekening koran');
INSERT INTO `barang` VALUES (24, 'setoran transfer rangkap 3');
INSERT INTO `barang` VALUES (65, 'setoran/transfer/kliring/inkaso');
INSERT INTO `barang` VALUES (34, 'speciment TT');
INSERT INTO `barang` VALUES (115, 'sticker DALAM PENGAWASAN (CRC)');
INSERT INTO `barang` VALUES (79, 'sticker registrasi');
INSERT INTO `barang` VALUES (80, 'sticker registrasi (spotlight)');
INSERT INTO `barang` VALUES (116, 'sticker sealed uang');
INSERT INTO `barang` VALUES (108, 'sticker sortir uang (Cab KIC)');
INSERT INTO `barang` VALUES (103, 'stomap gantung (pink)');
INSERT INTO `barang` VALUES (104, 'stop map 2color (biru-kuning)');
INSERT INTO `barang` VALUES (85, 'stop map rekening giro');
INSERT INTO `barang` VALUES (86, 'stopmap rekening mandiri');
INSERT INTO `barang` VALUES (98, 'surat pernyataan TPDPN');
INSERT INTO `barang` VALUES (33, 'syarat khusus');
INSERT INTO `barang` VALUES (125, 'syarat khusus Giro (badan)');
INSERT INTO `barang` VALUES (77, 'syarat khusus Giro Rupiah');
INSERT INTO `barang` VALUES (76, 'syarat khusus pemb.rek.tabungan');
INSERT INTO `barang` VALUES (97, 'syarat khusus rekening giro (2 s');
INSERT INTO `barang` VALUES (100, 'syarat khusus rekening tabungan');
INSERT INTO `barang` VALUES (32, 'syarat umum');
INSERT INTO `barang` VALUES (66, 'syarat umum pemb.rek.tabungan');
INSERT INTO `barang` VALUES (28, 'transaksi antar teller');
COMMIT;

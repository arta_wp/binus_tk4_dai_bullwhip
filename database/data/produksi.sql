/*
 Navicat Premium Data Transfer

 Source Server         : MySQL Local
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : sibangsat

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 31/10/2022 10:05:56
*/

-- ----------------------------
-- Records of produksi
-- ----------------------------
BEGIN;
INSERT INTO `produksi` VALUES (69, 96, 38, '600', '1');
INSERT INTO `produksi` VALUES (70, 70, 38, '1000', '3');
INSERT INTO `produksi` VALUES (71, 69, 37, '1000', '3');
INSERT INTO `produksi` VALUES (72, 69, 37, '1000', '3');
INSERT INTO `produksi` VALUES (73, 97, 37, '600', '2');
INSERT INTO `produksi` VALUES (74, 90, 31, '1050', '3');
INSERT INTO `produksi` VALUES (75, 79, 31, '1000', '3');
INSERT INTO `produksi` VALUES (76, 63, 31, '1000', '3');
INSERT INTO `produksi` VALUES (77, 76, 29, '55', '1');
INSERT INTO `produksi` VALUES (78, 88, 29, '50', '1');
INSERT INTO `produksi` VALUES (79, 61, 29, '50', '1');
INSERT INTO `produksi` VALUES (80, 94, 35, '5000', '4');
INSERT INTO `produksi` VALUES (81, 67, 35, '6000', '4');
INSERT INTO `produksi` VALUES (82, 68, 36, '6000', '4');
INSERT INTO `produksi` VALUES (83, 95, 36, '5000', '4');
INSERT INTO `produksi` VALUES (84, 57, 25, '98', '1');
INSERT INTO `produksi` VALUES (85, 72, 25, '200', '1');
INSERT INTO `produksi` VALUES (86, 84, 25, '50', '1');
INSERT INTO `produksi` VALUES (87, 62, 30, '30', '1');
INSERT INTO `produksi` VALUES (88, 77, 30, '30', '1');
INSERT INTO `produksi` VALUES (89, 89, 30, '30', '1');
INSERT INTO `produksi` VALUES (90, 86, 27, '30', '1');
INSERT INTO `produksi` VALUES (91, 59, 27, '40', '1');
INSERT INTO `produksi` VALUES (92, 73, 26, '100', '1');
INSERT INTO `produksi` VALUES (93, 85, 26, '50', '1');
INSERT INTO `produksi` VALUES (94, 58, 26, '100', '1');
INSERT INTO `produksi` VALUES (95, 78, 39, '100', '1');
INSERT INTO `produksi` VALUES (96, 71, 24, '200', '1');
INSERT INTO `produksi` VALUES (97, 83, 24, '200', '1');
INSERT INTO `produksi` VALUES (98, 56, 24, '200', '1');
INSERT INTO `produksi` VALUES (99, 93, 34, '1000', '3');
INSERT INTO `produksi` VALUES (100, 82, 34, '1000', '3');
INSERT INTO `produksi` VALUES (101, 66, 34, '200', '1');
INSERT INTO `produksi` VALUES (102, 65, 33, '500', '2');
INSERT INTO `produksi` VALUES (103, 81, 33, '1000', '1');
INSERT INTO `produksi` VALUES (104, 91, 32, '500', '2');
INSERT INTO `produksi` VALUES (105, 80, 32, '500', '2');
INSERT INTO `produksi` VALUES (106, 64, 32, '500', '2');
INSERT INTO `produksi` VALUES (107, 92, 32, '1000', '3');
INSERT INTO `produksi` VALUES (108, 92, 32, '1000', '3');
INSERT INTO `produksi` VALUES (109, 87, 28, '50', '1');
INSERT INTO `produksi` VALUES (110, 60, 28, '100', '1');
INSERT INTO `produksi` VALUES (111, 75, 28, '50', '1');
INSERT INTO `produksi` VALUES (112, 97, 37, '300', '1');
INSERT INTO `produksi` VALUES (113, 56, 24, '210', '2');
INSERT INTO `produksi` VALUES (114, 56, 24, '210', '2');
INSERT INTO `produksi` VALUES (115, 56, 24, '210', '2');
COMMIT;

/*
 Navicat Premium Data Transfer

 Source Server         : MySQL Local
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : sibangsat

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 31/10/2022 10:05:41
*/



-- ----------------------------
-- Records of pemesanan
-- ----------------------------
BEGIN;
INSERT INTO `pemesanan` VALUES (56, 'mandiri cabang pahlawan', 24, '200', 1);
INSERT INTO `pemesanan` VALUES (57, 'mandiri cabang pahlawan', 25, '100', 1);
INSERT INTO `pemesanan` VALUES (58, 'mandiri cabang pahlawan', 26, '50', 1);
INSERT INTO `pemesanan` VALUES (59, 'mandiri cabang pahlawan', 27, '50', 1);
INSERT INTO `pemesanan` VALUES (60, 'mandiri cabang pahlawan', 28, '50', 1);
INSERT INTO `pemesanan` VALUES (61, 'mandiri cabang pahlawan', 29, '25', 1);
INSERT INTO `pemesanan` VALUES (62, 'mandiri cabang pahlawan', 30, '20', 1);
INSERT INTO `pemesanan` VALUES (63, 'mandiri cabang pahlawan', 31, '450', 0);
INSERT INTO `pemesanan` VALUES (64, 'mandiri cabang pahlawan', 32, '500', 0);
INSERT INTO `pemesanan` VALUES (65, 'mandiri cabang pahlawan', 33, '500', 0);
INSERT INTO `pemesanan` VALUES (66, 'mandiri cabang pahlawan', 34, '500', 0);
INSERT INTO `pemesanan` VALUES (67, 'mandiri cabang pahlawan', 35, '6000', 0);
INSERT INTO `pemesanan` VALUES (68, 'mandiri cabang pahlawan', 36, '6000', 0);
INSERT INTO `pemesanan` VALUES (69, 'mandiri cabang pahlawan', 37, '500', 0);
INSERT INTO `pemesanan` VALUES (70, 'mandiri cabang pahlawan', 38, '500', 0);
INSERT INTO `pemesanan` VALUES (71, 'mandiri cabang pandanaran', 24, '100', 0);
INSERT INTO `pemesanan` VALUES (72, 'mandiri cabang pandanaran', 25, '100', 0);
INSERT INTO `pemesanan` VALUES (73, 'mandiri cabang pandanaran', 26, '50', 0);
INSERT INTO `pemesanan` VALUES (74, 'mandiri cabang pandanaran', 27, '50', 0);
INSERT INTO `pemesanan` VALUES (75, 'mandiri cabang pandanaran', 28, '50', 0);
INSERT INTO `pemesanan` VALUES (76, 'mandiri cabang pandanaran', 29, '20', 0);
INSERT INTO `pemesanan` VALUES (77, 'mandiri cabang pandanaran', 30, '20', 0);
INSERT INTO `pemesanan` VALUES (78, 'mandiri cabang pandanaran', 39, '50', 0);
INSERT INTO `pemesanan` VALUES (79, 'mandiri cabang pandanaran', 31, '500', 0);
INSERT INTO `pemesanan` VALUES (80, 'mandiri cabang pandanaran', 32, '500', 0);
INSERT INTO `pemesanan` VALUES (81, 'mandiri cabang pandanaran', 33, '500', 0);
INSERT INTO `pemesanan` VALUES (82, 'mandiri cabang pandanaran', 34, '500', 0);
INSERT INTO `pemesanan` VALUES (83, 'mandiri cabang candi baru', 24, '100', 0);
INSERT INTO `pemesanan` VALUES (84, 'mandiri cabang candi baru', 25, '50', 0);
INSERT INTO `pemesanan` VALUES (85, 'mandiri cabang candi baru', 26, '30', 0);
INSERT INTO `pemesanan` VALUES (86, 'mandiri cabang candi baru', 27, '30', 0);
INSERT INTO `pemesanan` VALUES (87, 'mandiri cabang candi baru', 28, '30', 0);
INSERT INTO `pemesanan` VALUES (88, 'mandiri cabang candi baru', 29, '20', 0);
INSERT INTO `pemesanan` VALUES (89, 'mandiri cabang candi baru', 30, '20', 0);
INSERT INTO `pemesanan` VALUES (90, 'mandiri cabang candi baru', 31, '500', 0);
INSERT INTO `pemesanan` VALUES (91, 'mandiri cabang candi baru', 32, '500', 0);
INSERT INTO `pemesanan` VALUES (92, 'mandiri cabang candi baru', 32, '500', 0);
INSERT INTO `pemesanan` VALUES (93, 'mandiri cabang candi baru', 34, '500', 0);
INSERT INTO `pemesanan` VALUES (94, 'mandiri cabang candi baru', 35, '6000', 0);
INSERT INTO `pemesanan` VALUES (95, 'mandiri cabang candi baru', 36, '6000', 0);
INSERT INTO `pemesanan` VALUES (96, 'mandiri cabang candi baru', 38, '300', 0);
INSERT INTO `pemesanan` VALUES (97, 'mandiri cabang candi baru', 37, '300', 0);
INSERT INTO `pemesanan` VALUES (98, 'mandiri', 101, '12', 0);
COMMIT;


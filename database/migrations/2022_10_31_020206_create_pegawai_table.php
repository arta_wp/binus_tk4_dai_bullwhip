<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            $table->id('id_pegawai')->index();
            $table->string('username');
            $table->string('password');
            $table->string('nama_pegawai');
            $table->text('alamat_pegawai');
            $table->text('hp_pegawai');
            $table->unsignedBigInteger('id_bagian');
            $table->foreign('id_bagian')->references('id_bagian')->on('bagian');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegawai');
    }
};

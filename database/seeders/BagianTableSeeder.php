<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Bagian;

class BagianTableSeeder extends Seeder
{
    public function run()
    {
        $bagian = [
            [ 
                'id_bagian' => 1,
                'nama_bagian' => 'Administrator'
            ],
            [ 
                'id_bagian' => 2,
                'nama_bagian' => 'Gudang'
            ],
            [ 
                'id_bagian' => 3,
                'nama_bagian' => 'Manajer'
            ],
            [ 
                'id_bagian' => 4,
                'nama_bagian' => 'Pesanan'
            ],
            [ 
                'id_bagian' => 5,
                'nama_bagian' => 'Produksi'
            ]
        ];

        Bagian::insert($bagian);
    }
}

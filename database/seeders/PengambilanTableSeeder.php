<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Pengambilan;

class PengambilanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pengambilan = [
            [ 
                'id_pengambilan' => 4,
                'nama_pengambil' => 'mandiri cabang pahlawan',
                'id_barang' => 24,
                'jumlah_pengambilan' => 200
            ],
            [ 
                'id_pengambilan' => 5,
                'nama_pengambil' => 'mandiri cabang pahlawan',
                'id_barang' => 25,
                'jumlah_pengambilan' => 100
            ]
        ];

        Pengambilan::insert($pengambilan);
    }
}

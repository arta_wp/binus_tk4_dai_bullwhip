<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\Pegawai;

class PegawaiTableSeeder extends Seeder
{
    public function run()
    {
        $pegawai = [
            [ 
                'id_pegawai' => 1,
                'username' => 'andri', 
                'password' => Hash::make('12345678'), 
                'nama_pegawai' => 'Andri Gunawan', 
                'alamat_pegawai' => 'Bandung', 
                'hp_pegawai' => '082123123123', 
                'id_bagian' => 1
            ],
            [ 
                'id_pegawai' => 2,
                'username' => 'ahmad', 
                'password' => Hash::make('12345678'), 
                'nama_pegawai' => 'Ahmad Fariz Fistiandana', 
                'alamat_pegawai' => 'Jakarta', 
                'hp_pegawai' => '082123123345', 
                'id_bagian' => 2
            ],
            [ 
                'id_pegawai' => 3,
                'username' => 'arta', 
                'password' => Hash::make('12345678'), 
                'nama_pegawai' => 'Arta Windy Pratama', 
                'alamat_pegawai' => 'Bali', 
                'hp_pegawai' => '082123123100', 
                'id_bagian' => 3
            ],
            [ 
                'id_pegawai' => 4,
                'username' => 'fajar', 
                'password' => Hash::make('12345678'), 
                'nama_pegawai' => 'Fajar Cipto Kusumo', 
                'alamat_pegawai' => 'Bali', 
                'hp_pegawai' => '082123123212', 
                'id_bagian' => 4
            ],
            [ 
                'id_pegawai' => 5,
                'username' => 'wening', 
                'password' => Hash::make('12345678'), 
                'nama_pegawai' => 'Wening Tyas Utami', 
                'alamat_pegawai' => 'Surabaya', 
                'hp_pegawai' => '082123123212', 
                'id_bagian' => 5
            ]
        ];

        Pegawai::insert($pegawai);
    }
}
